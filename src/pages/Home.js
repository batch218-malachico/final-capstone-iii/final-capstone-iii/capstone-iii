import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
// import Footer from '../components/Footer';


export default function Home() {

	const data = {
		title: "Code Pizza",
		content: "Life is uncertain. But pizza is always a sure thing.",
		destination: "/courses",
		label: "Order Now!"
	}

	return (
		<div className="home-page">
		<Banner data={data} />
    	<Highlights />
    	{/*<Footer />*/}
		</div>
	)
}

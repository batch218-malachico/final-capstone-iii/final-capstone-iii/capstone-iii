import CourseCard from '../components/CourseCard';
import {useState, useEffect} from 'react';
import Footer from "../components/Footer";

export default function Courses() {

	const [courses, setCourses] = useState([]);

	
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/active`)
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setCourses(data.map(course => {
				return (
					<CourseCard key={course._id} course={course} />
				)
			}))
		})
	}, [])

	return(
		<>
		{courses}
		{/*<Footer />*/}
		</>
	)
}

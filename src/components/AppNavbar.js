
import {useContext} from 'react';

import {Link, NavLink} from 'react-router-dom';

import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

import UserContext from '../UserContext';


export default function AppNavbar() {

  const { user } = useContext(UserContext);

  return (
  <div>
    <Navbar bg="warning" expand="lg">
        <div className="logo">
        <Navbar.Brand as={Link} to="/">Code Pizza</Navbar.Brand>
        </div>

        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ml-auto">
            <Nav.Link as={NavLink} to="/">Home</Nav.Link>
            <Nav.Link as={NavLink} to="/courses">Menu</Nav.Link>

            {(user.id !== null) ? 
                <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
                :
                <>
                <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
                <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
                </>
            }
          </Nav>
        </Navbar.Collapse>
    </Navbar>
  </div>
  );
}



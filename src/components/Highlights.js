import {Row, Col , Card} from 'react-bootstrap';


export default function Highlights() {
	return (
		<div style={{paddingTop: "25px", paddingRight:"25px", paddingLeft: "25px", paddingBottom: "35px"}}>
	    <Row className="mt-5 mb-2">
	        <Col xs={12} md={3}>
	            <Card className="cardHighlight p-3 bg-warning">
	                <Card.Body>
	                    <Card.Title>
	                        <h4 class="text-center">Best for Family</h4>
	                    </Card.Title> <br/>
	                    <Card.Subtitle className="mb-2 text-muted" class="text-center"><strong>Hawaiian Pizza</strong></Card.Subtitle>
	                    <br/>
	                    <Card.Text class="text-center"> 
	                        Classic Hawaiian pizza combines pizza sauce, cheese, cooked ham, and pineapple.
	                    </Card.Text>
	                </Card.Body>
	            </Card>
	        </Col>
	        <Col xs={12} md={3}>
	            <Card className="cardHighlight p-3 bg-warning">
	                <Card.Body>
	                    <Card.Title>
	                        <h4 class="text-center">Best for Lovers</h4>
	                    </Card.Title> <br/>
	                    <Card.Subtitle className="mb-2 text-muted" class="text-center"><strong>Cheesy Overload Pizza</strong></Card.Subtitle> <br/>
	                    <Card.Text class="text-center">
	                        Topped with provolone, mozzarella, asiago, cheddar, and romano cheeses.
	                    </Card.Text>
	                </Card.Body>
	            </Card>
	        </Col>
	        <Col xs={12} md={3}>
	            <Card className="cardHighlight p-3 bg-warning">
	                <Card.Body>
	                    <Card.Title>
	                        <h4 class="text-center">Best for Barkadas</h4>
	                    </Card.Title> <br/>
	                    <Card.Subtitle className="mb-2 text-muted" class="text-center"><strong>Pepperoni Pizza</strong></Card.Subtitle> <br/>
	                    <Card.Text class="text-center">
	                       Topped with ham, pepperoni, bacon, Italian sausage, burger crumbles, and Spanish sausage plus veggies, cheese, pineapples and mushrooms.
	                    </Card.Text>
	                </Card.Body>
	            </Card>
	        </Col>
	        <Col xs={12} md={3}>
	            <Card className="cardHighlight p-3 bg-warning">
	                <Card.Body>
	                    <Card.Title>
	                        <h4 class="text-center">Health Conscious</h4>
	                    </Card.Title> <br/>
	                    <Card.Subtitle className="mb-2 text-muted" class="text-center"><strong>Vegetable Pizza</strong></Card.Subtitle> <br/>
	                    <Card.Text class="text-center">
	                       Topped with fresh tomatoes, onions, arugula, kale, eggplants, bell peppers, spinach, zucchini, mushrooms.
	                    </Card.Text>
	                </Card.Body>
	            </Card>
	        </Col>
	    </Row>
	    </div>
	)
}
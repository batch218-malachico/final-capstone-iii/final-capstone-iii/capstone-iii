
import { Button, Row, Col, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import {Container} from 'react-bootstrap';
import './Banner.css';

export default function Banner({data}) {

    const {title, content, destination, label} = data;

return (
<Container className="parent">
    <div className="mt-4, mb-4">
        <Col xs={12} md={12}>
            <Card className="cardHighlight p-3 bg-warning">
                <Card.Body className="text-center">
                    <Card.Title>
                        <h4 class="text-center">{title}</h4>
                    </Card.Title>
                    <Card.Text class="text-center"> 
                        {content}
                    </Card.Text>
                    <div class="text-center">
                    <Button className="bg-dark" as={Link} to={destination}>{label}</Button>
                    </div>
                </Card.Body>
            </Card>
        </Col>
    </div>
</Container> 
    )
}


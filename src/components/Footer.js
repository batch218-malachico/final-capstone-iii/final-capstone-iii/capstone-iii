import React from 'react';
import {Container} from 'react-bootstrap';

import './Footer.css';

export default function Highlights() {

	 return (
	 <Container>
	    <div className="footer">
	      <footer>
	      	<h4>All Rights Reserve</h4>
	        <p>Copyright © {new Date().getFullYear()} Code Pizza</p>
	      </footer>
	    </div>
	  </Container>
	  );
	
}